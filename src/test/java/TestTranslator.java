import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import utwente.celsiusfahrenheittranslator.Translator;

public class TestTranslator {
    @Test
    public void testbook1() throws Exception {
        Translator translator = new Translator();
        double price = translator.getFahrenheit(10);
        Assertions.assertEquals(50.0, price, 0.0, "Temperature in Fahrenheit");
    }
}