package utwente.celsiusfahrenheittranslator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Translator extends HttpServlet {
    public void doGet(HttpServletRequest request,
                      HttpServletResponse respons) throws ServletException, IOException {

        respons.setContentType("text/html");
        PrintWriter out = respons.getWriter();
        String docType = "<!DOCTYPE HTML>\n";
        String title = "Celsius to Fahrenheit";
        double fahrenheit = getFahrenheit(Double.parseDouble(request.getParameter("celsius")));
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in Celsius: " +
                request.getParameter("celsius") + "\n" +
                "  <P>Temperature in Fahrenheit: " + fahrenheit +
                "</BODY></HTML>");
    }
    public double getFahrenheit(double value) {
        return Math.round(((value * 1.8) + 32) * 100.0) / 100.0;
    }
}
